# Define the terraform providers required
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}


# Define AWS settings
provider "aws" {
  profile = "default"
  region  = "eu-central-1"
}


# Creating a new user group
resource "aws_iam_group" "example_group" {
  name = "ExampleMiklosGroup"
}


# Creating a new user
resource "aws_iam_user" "miklos_user" {
  name = "MiklosAdmin"
}


# Attach the AWS managed AdministratorAccess policy to the ExampleMiklosGroup group
resource "aws_iam_policy_attachment" "admin_policy_attachment" {
  name       = "AdminPolicyAttachment"
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"  # AWS managed AdministratorAccess policy ARN
  groups     = [aws_iam_group.example_group.name]
}


# Adding the new user to this newly created group
resource "aws_iam_user_group_membership" "miklos_user_group_membership" {
  user   = aws_iam_user.miklos_user.name
  groups = [aws_iam_group.example_group.name]
}


# Create a random ID for the EC2 instance name
resource "random_id" "server" {
  byte_length = 8
}


# Create an IAM role with administrative privileges
resource "aws_iam_role" "admin_role" {
  name = "AdminRole2"
  
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      }
    ]
  })
}


# Attach necessary policies for administrative access
resource "aws_iam_policy_attachment" "admin_attach" {
  name       = "AdminAttach"
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
  roles      = [aws_iam_role.admin_role.name]
}


# Create an IAM instance profile for the EC2 instance
resource "aws_iam_instance_profile" "server" {
  name = "EC2InstanceProfile"
  role = aws_iam_role.admin_role.name
}


# Create EC2 instance with the IAM instance profile
resource "aws_instance" "server" {
  ami           = "ami-0c4c4bd6cf0c5fe52"
  instance_type = "t2.micro"
  iam_instance_profile = aws_iam_instance_profile.server.name
  tags = {
    Name = "ExampleInstanceMiklos-${random_id.server.hex}"
  }
}


# Adding a new bucket to my account
resource "aws_s3_bucket" "exposedbucketm" {
  # Bucket name:
  bucket = "accidentlyexposedm"

  # Bucket tags
  tags = {
    Name        = "Exposed Bucket M"
    Environment = "Dev"
  }
}


resource "aws_s3_bucket_versioning" "exposedbucketm" {
  bucket = aws_s3_bucket.exposedbucketm.id
  
  versioning_configuration {
    status = "Disabled"
  }
}


resource "aws_s3_bucket_public_access_block" "exposedbucketm" {
  bucket = aws_s3_bucket.exposedbucketm.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}


resource "aws_codecommit_repository" "example_repo" {
  repository_name = "Miklos_Example_CodeCommit_Repo"
  description     = "My example CodeCommit repository"
}


output "repository_clone_url_http" {
  value = aws_codecommit_repository.example_repo.clone_url_http
}


output "repository_clone_url_ssh" {
  value = aws_codecommit_repository.example_repo.clone_url_ssh
}



resource "aws_s3_bucket" "example_bucket2" {
  bucket = "my-example-bucket"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}

resource "aws_s3_bucket_object" "example_object2" {
  bucket = aws_s3_bucket.example_bucket2.bucket
  key    = "someobject"
  content = "hardcoded-secret-API_KEY=12345"
}

variable "hardcoded_password" {
  description = "This is an example of a hardcoded password, which is not recommended"
  default     = "SuperSecretPassword123!"
}
